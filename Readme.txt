****FinPlan****

"An application for financial advisors to collect & maintain client records, engagements, interactions and policies remotely and securely on their internet enabled device"

***************


***Getting Started****

This is the base repository for the FinPlan application we are devloping under Scrum for our Software process managment module.

Prerequisites:
Python 3 										:https://python.org/downloads/
pip					:python package installer	:https://pypi.python.org/pypi/pip
bottle.py			:pip install bottle			:https://bottlepy.org/
bootstrap 3.0		:included via CDN			:https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
Bootstrap Validator	:included via CDN 			:https://1000hz.github.io/bootstrap-validator/


*bottle.py is a addon module for python that will allow us to deploy and test locally a python http server.


To get started:
-clone This git repository to a directory of your choosing
-pip install bottle
-run the finplan.py file python finplan.py
-open any internet browser to "http://localhost:8080/finplan"
-username: "user"
-password: "pass"


****************


