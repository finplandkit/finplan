<!DOCTYPE html5>
<html lang="en">
  <head>
		%include('_template/scripts')
	  </head>
	  <body>
		%include('_template/navbar')
		<!--   This is where your HTML code STARTS -->
	
		<div class="container">
			
			<script>
			$(document).ready(function(){
			$('#sel_button').hide();
			
			$('#new_search').click(function(e){
			window.location.href = "/Client Search";
			
			});
			
			$('#thetable').find('tr').click( function(){
				
				var row = $(this).find('td:first').text();
				$('#client_id').attr('value',client_id.toString());
				
				$('#sel_button').show();
				
				});
				
				
				
		
			});
						
			</script>
		  
			<h1>Client Search Results</h1>
			<br/>
				
			<form id='search_results' name ="search_results" action="/Select_client" method="POST">
			
			<input type="hidden" id="client_id" name="client_id" value=0/>
				<div class="form-group">
					<table class="table table-hover" id='thetable'>
						<tr>
							<th>ID</th><th>FirstName</th><th>LastName</th><th>DoB</th><th>Gender</th>
							<th>Address(line_1)</th><th>Address(line_2)</th><th>Town</th><th>County</th><th>Eircode</th>
							<th>Email</th><th>Mobile</th><th>Other</th><th>Client ID</th>
						</tr>
						%for row in rows:
							<tr>
							%for col in row:
								<td>{{col}}</td>
							%end
							<!--<td><button id='select_{{row[0]}}' class="btn btn-success selector" onclick="window.location.href ='/action/client_select/{{row[0]}}' ">Select</button></td> -->
							<td>
							</tr>
						%end
					</table>
					
				</div><button id='sel_button' class="btn btn-success selector pull-right" type='submit' hidden>Select</button></td>
				</form>
			  
				<div>
					<button id="new_search" class="btn btn-primary">New Search</button></a>
				</div>
			  
			
		
		</div>
		
		 
		
		
		<!--   This is where your HTML code ENDS -->
	</body>
</html>