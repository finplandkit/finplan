
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header"> <a class="navbar-brand" href="/home">FIN PLAN</a> </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        %pages = ['Client Search','Add Client','Employment Details','Income Details','Expenditure Details','Product Types' ,'Product Details']
        %for page in pages:
        <li><a href="{{page}}">{{page}}</a></li>
        %end
      </ul>
      <script>
	  $(document).ready(function(){
	  		 document.title= $("h1").text();
			  var title = document.title;
			  $('#myNavbar').find('li').each(function(index, element) {
                if($(this).text()== title){
					$(this).attr('class','active');}else{ $(this).attr('class','');}
           			 });
			});
              </script>
      <ul class="nav navbar-nav navbar-right">
	  <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
    </div>
  </div>
</nav>
