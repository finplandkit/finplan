<!DOCTYPE html>
<html lang="en">
  <head>
		%include('_template/scripts')
	  </head>
	  <body>
		%include('_template/navbar')
		<!--   This is where your HTML code STARTS -->
		<div class="container">
		
		<script>
		  
			var QUESTIONS =[];
			var COUNTER =0;
			var initializationFlag = 0;
			
			var clientID = 0;
			var userID = 0;
			var productID = 0;
			
			//question object
			function question(id, label, name, type, placeholder, pattern, product_id){
				var q = new Object();
				q.id = id;
				q.label = label;
				q.name = name;
				q.type = type;
				q.placeholder = placeholder;
				q.pattern = pattern;
				q.product_id = product_id;
				return q;
			}
			
			
			function initialize(){
				var $div = $("#questionsDiv");
				
				if(initializationFlag == 0){
				
					%for row in rows:
						var q = question('{{row[0]}}','{{row[1]}}','{{row[2]}}','{{row[3]}}','{{row[4]}}','{{row[5]}}','{{row[6]}}');
						QUESTIONS.push(q);
						$div.append(new_question_div(q))
					%end
					
					%clientID = client_id;
					%userID = user_id;
					%productID = product_id;
					
					$("#clientID").attr("value", '{{clientID}}');
					$("#userID").attr("value",'{{userID}}');
					$("#productID").attr("value",'{{productID}}');
					initializationFlag = 1
				
				}
			}
			
			function new_question_div(q){
				var qNum = ++COUNTER;
				
				var $questionDiv = $("<div/>");
			
				$questionDiv.append(
						$('<label/>').text("Q" + qNum + ": " + q.label).attr("for", "" + q.name)
					)
					.append(
						$('<input/>').attr("id",q.name).attr("name",q.name).attr("type",q.type).attr("placeholder",q.placeholder).attr("pattern",q.pattern).attr("required","").addClass("form-control")
					);
				
				return $questionDiv
			}
			
			
			
			$(document).ready(function(){
			
				initialize()
				$("#questionsDiv").validator('update');
				
				$('#Product_Name').find('h3').text('{{product_details[0][0]}}');
				$('#product_details').find('p').text('{{product_details[0][1]}}');
				
				
			});
			
		  </script>
		  
		  <h1>Product Details</h1>
		  <p>A detailed description of the selected product.</p> 
		  <div id="Product_Name">
		  <h3 >#Product Selected#</h3><br>
		  
		  </div>
		  <div id="product_details" class="well well-sm"><p>#description#</p>
		  </div>
		  
		  
		  
		  <h3>Additonal Info Required</h3>
		  <form role="form" data-toggle="validator" action="/Product Details" method="POST">

		  <div id="questionsDiv" class="form-group well well-sm">
			<!--   Questions will be pulled from the database -->
		  </div>
		  <div><button type="submit" class="btn btn-default">Submit</button></div>
		  <input type="hidden" id="userID" value=0 name ="userID"></input>
		  <input type="hidden" id="clientID" value=0 name ="clientID"></input>
		  <input type="hidden" id="productID" value=0 name ="productID"></input>
		  </form>
		</div>
		<!--   This is where your HTML code ENDS -->
</body>
</html>