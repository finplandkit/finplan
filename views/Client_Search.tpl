<!DOCTYPE html>
<html lang="en">
  <head>
		%include('_template/scripts')
	  </head>
	  <body>
		%include('_template/navbar')
		<!--   This is where your HTML code STARTS -->
	
		<div class="container">
			
			<script>
			
				var initFlag = 0;
				
				$(document).ready(function(){
					
					initialize();
					
					$('input[type=radio][name=searchTypeSelection]').change(function() {
						
						$("#searchDiv").empty();
						
						if (this.value == 'Client ID') {
						
							clientIDSearchDiv();
						}
						else if (this.value == 'Client Details') {
							
							clientDetailsSearchDiv();
						}
					});
					
					$("#client_search_form").validator('validate');
				});
			
				function initialize(){
					
					if(initFlag == 0){
						clientDetailsSearchDiv();
						initFlag = 1;
					}
				}
				
				function clientIDSearchDiv(){
				
					var $searchDiv = $("#searchDiv");
					
					var $div = $("<div/>");
					$div.append(
						$('<label/>').text("Client ID:").attr("for", "clientId")
					)
					.append(
						$('<input/>').attr("id","clientId").attr("name","clientId").attr("type","text").attr("placeholder","Enter client ID here").attr("pattern","^[a-zA-Z0-9]*$ ").addClass("form-control")
					);
					
					$searchDiv.append($div);
					$searchDiv.append("<hr>");
				
				}
			
				function clientDetailsSearchDiv(){
				
					var $searchDiv = $("#searchDiv");
					
					var $div = $("<div/>");
					$div.append(
						$('<label/>').text("Last Name:").attr("for", "lastName")
					)
					.append(
						$('<input/>').attr("id","lastName").attr("name","lastName").attr("type","text").attr("pattern","^[A-Za-z\s]+$").attr("placeholder","Enter last name here").addClass("form-control")
					);
					
					$searchDiv.append($div);
					
					var $div = $("<div/>");
					$div.append(
						$('<label/>').text("First Name:").attr("for", "firstName")
					)
					.append(
						$('<input/>').attr("id","firstName").attr("name","firstName").attr("type","text").attr("pattern","^[A-Za-z\s]+$").attr("placeholder","Enter last name here <optional>").addClass("form-control")
					);
					
					$searchDiv.append($div);
					
					var $div = $("<div/>");
					$div.append(
						$('<label/>').text("Date of Birth:").attr("for", "dob")
					)
					.append(
						$('<input/>').attr("id","dob").attr("name","dob").attr("type","date").attr("placeholder","DD/MM/YYYY").attr("pattern","^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$").addClass("form-control")
					);
					
					$searchDiv.append($div);
					$searchDiv.append("<hr>");
					
				}
			
		  
			
			</script>
		  
			<h1>Client Search</h1>
			<br/>
				
			<form role="form" id="client_search_form" data-toggle='validator'  action="/Search_Results" method ="POST">
			  
				<div>
					<label> Search by: </label>
					<label class="radio-inline"><input type="radio" name="searchTypeSelection" value="Client Details" checked>Client Details</label>
					<label class="radio-inline"><input type="radio" name="searchTypeSelection" value="Client ID">Client ID</label>
					<hr>
				</div>
					
				<div id="searchDiv" >
				
				</div>
			  
				<div>
					<button type="submit" class="btn btn-primary">Search</button>
				</div>
			  
			</form>
		
		</div>
		
		 
		
		
		<!--   This is where your HTML code ENDS -->
	</body>
</html>