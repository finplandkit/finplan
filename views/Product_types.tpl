<!DOCTYPE html>
<html lang="en">
  <head>
		%include('_template/scripts')
	  </head>
	  <body>
		%include('_template/navbar')
		<!--   This is where your HTML code STARTS -->
<script>
		
		//array for products
		var products =[[],[],[]];
		
		//product object
		function product(id,type,name,description,tooltip){
		this.id=id;
		this.type= type;
		this.name=name;
		this.description=description;
		this.tooltip = tooltip;
		
		
		}
		
		
		$(document).ready(function(){
		
		var list1 = $('#protection_list');
		var list2 = $('#pension_list');
		var list3 = $('#savings_list');
	
		$('#submit_button').hide();
		
		//Get the products provided by our server
		getProducts();
		add_products(list1,products[0]);
		add_products(list2,products[1]);
		add_products(list3,products[2]);
		
		$('[data-toggle="tooltip"]').tooltip();
		});
	
		
		
		function add_products(list,row){
		
			for (n in row){
			
				add_product_to_list(list,row[n]);
			}
		
		
		}
		
		
		function add_product_to_list(list,n){

				list.append(
				
					$('<span/>')
					.attr('title',n.tooltip)
					.attr('data-toggle',"tooltip")
					.attr('data-placement','bottom')
					.text(n.name)
					.addClass("list-group-item product pull-left col-xs-12 ")
					.click(function(e){
						var span = $(this);
						span.siblings().removeClass('active').end().addClass('active');
						e.preventDefault();
						$('#selected_product').attr("value",n.id);
						$('#submit_button').show();
					}).append(
					$("<span/>")
					.addClass("glyphicon glyphicon-info-sign pull-right")
					.on("click",function(e){
					e.preventDefault();
						<!-- modal used for description--!>
						var info_modal = $("#myModal");
						$(".modal-title").text(n.name);
						$(".modal-body").html("<p>"+n.description+"</p>");

						info_modal.modal('toggle');
					
					
					})

					)
				)
		
		
		
		}
		
		function getProducts(){
		
		%x =0
			%for row in products:
					%if row[1] =="Protection":
						%x=0;
						
					%elif row[1] == "Pension":
						%x=1
						
					%else:
						%x=2
						
					%end
						var prod = new product('{{row[0]}}','{{row[1]}}','{{row[2]}}','{{row[3]}}','{{row[4]}}');
						console.log(prod.name);
						products[{{x}}].push(prod);
						
				%end
	
		}
		
		
		
		</script>
		<div class="container">
		
		
		
		  <h1>Product Types</h1>
		  <p>Please choose the appropriate product type for your client</p> 
		 
			
			<div class="row-fluid row-centered" id = "product_types_div">
				<div class="col-xs-3 col-centered well well-lg text-center ">
				<button class="btn btn-warning btn-block" id='protection_button' data-toggle="collapse" data-target="#protection_list"><h4>Protection</h4></button>
				<ul class="list-group panel-collapse collapse" id="protection_list" >
						</ul>
				
				</div>
				<div class="col-xs-3 col-centered well well-lg text-center ">
					<button class="btn btn-warning btn-block"  id='pension_button'  data-toggle="collapse" data-target="#pension_list"><h4>Pension</h4></button>
					<ul class="list-group panel-collapse collapse"  id="pension_list" visibility="hidden">
				
				</ul>
				</div>
				<div class="col-xs-3 col-centered well well-lg text-center ">
					<button class="btn btn-warning btn-block" id='saving_button'  data-toggle="collapse" data-target="#savings_list"><h4>Savings & Investment</h4></button>
					<ul class="list-group panel-collapse collapse"   id="savings_list">
					
				</ul>
				</div>
				</div>
				
				
		</div>
		
			<div  class="container">
			<form visibility="hidden" data-toggle="validator" action="/Product Types" method="POST">
			<input type ="hidden" id="selected_product" required  name="product_id"></input>
			<button type="submit" class="btn btn-default" visibility="hidden" id="submit_button">Continue</button>
			 
		</form>
		
		<div class="container">



  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <p></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
		
		
		<!--   This is where your HTML code ENDS -->
</body>
</html>