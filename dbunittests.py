# -*- coding: utf-8 -*-
"""
Created on Sun Oct 23 16:02:31 2016

@author: Sean
"""

import unittest
import sqlite3 as lite
from databasefunctions import Client

class TestClient(unittest.TestCase):
    
        
    def test_insert(self):
        # Client test
        db = Client('FinplanDB.db', test = True)
        db.insertClient('Sean', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890')
        self.assertEqual(1, db.lastRowId())
        
        # sqlite connection
        expected_data = (1, 'Sean', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890', 'C1')
        row = ()
        test_query = '''SELECT id, firstname, lastname, strftime('%d/%m/%Y', dob), gender, addressline1, 
        addressline2, town, county, eircode, email, mobile, landline, clientId
        FROM Client WHERE id=?;'''
        con = lite.connect('FinplanDB.db')
        cursor = con.cursor()
        cursor.execute(test_query, (1,))
        row = cursor.fetchone()
        self.assertEqual(row, expected_data)
        
        #close connections
        db.dispose()
        if con:
            con.close()
        
    def test_update(self):
        # Client test
        db = Client('FinplanDB.db', test = True)
        db.insertClient('Sean', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890')
        db.updateClient('Tom', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890', db.lastRowId())
        db.dispose()
        
        # sqlite connection
        expected_data = (1, 'Tom', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890', 'C1')
        test_query = '''SELECT id, firstname, lastname, strftime('%d/%m/%Y', dob), gender, addressline1, 
        addressline2, town, county, eircode, email, mobile, landline, clientId
        FROM Client WHERE id=?;'''
        con = lite.connect('FinplanDB.db')
        cursor = con.cursor()
        cursor.execute(test_query, (1,))
        row = cursor.fetchone()
        self.assertEqual(row, expected_data)
        
        #close connections
        db.dispose()
        if con:
            con.close()
    
    def test_getClientById(self):
        # Client test
        db = Client('FinplanDB.db', test = True)
        db.insertClient('Sean', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890')
        row = db.getClientById(db.lastRowId())
        
        # sqlite connection
        expected_data = (1, 'Sean', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890', 'C1')
        test_query = '''SELECT id, firstname, lastname, strftime('%d/%m/%Y', dob), gender, addressline1, 
        addressline2, town, county, eircode, email, mobile, landline, clientId
        FROM Client WHERE id=?;'''
        con = lite.connect('FinplanDB.db')
        cursor = con.cursor()
        cursor.execute(test_query, (db.lastRowId(),))
        row = cursor.fetchone()
        self.assertEqual(row, expected_data)
        
        #close connections
        db.dispose()
        if con:
            con.close()
        
    def test_getAllClients(self):
        # Client test
        db = Client('FinplanDB.db', test = True)
        expected_data = (
            (1,'Sean', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890' ,'C1'),
            (2,'Tom', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890' ,'C2')
        )
        db.insertClient('Sean', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890')
        db.insertClient('Tom', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890')
        rows = db.getAllClients('''id, firstname, lastname, strftime('%d/%m/%Y', dob), gender, addressline1, addressline2, town, county, eircode, email, mobile, landline, clientId''')
        rowlist = []
        for row in rows:
            rowlist.append(row)
        rowlist = tuple(rowlist)
        self.assertEqual(rowlist, expected_data)
        
        # close connection
        db.dispose()
    
    def test_delete(self):
        # Client test
        
        db = Client('FinplanDB.db', test = True)
        expected_data = (2,'Tom', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890', 'C2')
        db.insertClient('Sean', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890')
        db.insertClient('Tom', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890')
        db.deleteClient(1) # delete rowid
        
        con = lite.connect('FinplanDB.db')
        cursor = con.cursor()
        test_query = '''SELECT id, firstname, lastname, strftime('%d/%m/%Y', dob), gender, addressline1, 
        addressline2, town, county, eircode, email, mobile, landline, clientId
        FROM Client WHERE id=?;'''
        cursor.execute(test_query, (2,))
        row = cursor.fetchone()
        self.assertEqual(row, expected_data)
        
        cursor.execute(test_query, (1,))
        row = cursor.fetchone()
        self.assertNotEqual(row, expected_data)
        
        #close connections
        db.dispose()
        if con:
            con.close()
        
        
        
if __name__ == '__main__':
    unittest.main()
    
    