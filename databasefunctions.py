# -*- coding: utf-8 -*-
"""
Created on Sun Oct 16 22:36:38 2016

@author: Sean
"""

from sqlcommand import SqlCommand
import datetime

    
class Client():
    
    __lastrowid = -1
    __lastrowcount = -1
    
    def lastRowId(self):
        return self.__lastrowid
        
    def lastRowCount(self):
        return self.__lastrowcount
    
    def count(self, rows):
        self.__rowList = []
        for row in rows:
            self.__rowList.append(row)
        return len(self.__rowList)
    
    def __init__(self, dbFile, test=False):
        self.__test = test
        self.command = SqlCommand(dbFile)
        if self.__test:
            self.command.execute('''DROP TABLE IF EXISTS Client''')
        self.command.execute(
        '''CREATE TABLE IF NOT EXISTS Client(id INTEGER PRIMARY KEY, firstname TEXT, 
        lastname TEXT, dob NUMERIC, gender TEXT, addressline1 TEXT, addressline2 TEXT,
        town  TEXT, county TEXT, eircode TEXT, email TEXT, mobile TEXT, landline TEXT,
        clientId TEXT);''')
    
    def insertClient(self, firstname, lastname, dob, gender, addressline1,
                     addressline2, town, county, eircode, email, mobile, landline):
        query = '''INSERT INTO Client(firstname, lastname, dob, gender, addressline1, 
        addressline2, town, county, eircode, email, mobile, landline)
        ValUES(?,?,?,?,?,?,?,?,?,?,?,?);'''
        self.__lastrowid = self.command.execute(query, [firstname, lastname, 
            dob, gender, addressline1, 
            addressline2, town, county, eircode, email, mobile, landline])
        query = '''UPDATE Client SET clientId=? WHERE id=?;'''
        self.command.execute(query, ['C{}'.format(self.__lastrowid), self.__lastrowid])
        
    def updateClient(self, firstname, lastname, dob, gender, addressline1, 
                     addressline2, town, county, eircode, email, mobile, landline, Id):
        query = '''UPDATE Client SET firstname=?, lastname=?, dob=?, gender=?, addressline1=?, 
        addressline2=?, town=?, county=?, eircode=?, email=?, mobile=?, landline=?
        WHERE id=?;'''
        self.command.execute(query, [firstname, lastname, 
            datetime.datetime.strptime(dob, "%d/%m/%Y"), gender, addressline1, 
            addressline2, town, county, eircode, email, mobile, landline, Id])    
    
    def deleteClient(self, Id):
        query = 'DELETE FROM Client WHERE id=?;'
        self.command.execute(query, [Id])
        
    def getAllClients(self, columns='*'):
        assert (isinstance(columns, str) and ('!--' not in columns)), 'Invalid columns specified'
        query = 'SELECT {} FROM Client;'.format(columns)
        rows = self.command.execute(query, [])
        self.__lastrowcount = self.count(rows)
        return tuple(self.__rowList)
        
    def getClientById(self, Id):
        query = 'SELECT * FROM Client WHERE id=?'
        rows = self.command.execute(query, (Id,))
        self.lastrowcount = self.count(rows)
        return tuple(self.__rowList)
        
    def dispose(self):
        self.command.closeConnection()



class Database:
    
    __tables = []
    
    def __init__(self, dbFile):
        self.command = SqlCommand(dbFile)
        self.__createTables()
        
        
    def __createTables(self):
        # insert table creation commands here
        #self.command.execute('DROP TABLE Friends');
        self.command.execute('CREATE TABLE IF NOT EXISTS Friends(id INTEGER PRIMARY KEY, Name TEXT, Age INT);')
        #end
        tableList = self.command.execute("SELECT name FROM sqlite_master WHERE type='table'",[])
        for tableName in tableList:
            self.__tables.append(tableName[0])
        print(self.__tables)
        
    def __hasTable(self, tableName):
        if tableName in self.__tables:
            return True
        return False
        
    def __getParameterString(self, parameters, cmd):
        if cmd == 'i':
            paramCount = parameters.count(',') + 1
            paramString = '?'
            if paramCount == 1:
                return paramString
            else:
                for i in range(1,paramCount):
                    paramString += ',?'
                    return paramString
        else:
            paramList = ''
            params = parameters.split(',')
            for parameter in params:
                parameter = parameter.strip()
                parameter += '=?,'
                paramList += parameter
            paramList = paramList[:-1]
            return paramList
        
    def insert(self, tableName, parameters, values):
        assert (isinstance(tableName, str) and ('!--' not in tableName)), 'Invalid tablename specified'
        assert (isinstance(parameters, str) and ('!--' not in parameters)), 'Invalid parameter(s) specified'
        assert (isinstance(values, list) and len(values) == parameters.count(',') + 1), 'parameter:value(list) length mismatch'
        assert (self.__hasTable(tableName)), 'Table: {}, does not exist!'.format(tableName)
        params = self.__getParameterString(parameters, cmd='i')
        query = 'INSERT INTO {0}({1}) VALUES({2});'.format(tableName, parameters, params)
        print(query)
        return self.command.execute(query, values)
    
    def update(self, tableName, parameters, values, Id):
        assert (isinstance(tableName, str) and ('!--' not in tableName)), 'Invalid tablename specified'
        assert (isinstance(parameters, str) and ('!--' not in parameters)), 'Invalid parameter(s) specified'
        assert (isinstance(values, list)), 'parameter:value(tuple) length mismatch'
        assert (self.__hasTable(tableName)), 'Table: {}, does not exist!'.format(tableName)
        params = self.__getParameterString(parameters, 'u')
        query = 'UPDATE {0} SET {1} WHERE id=?;'.format(tableName, params)
        print(query)
        values.append(Id)
        self.command.execute(query, values)
    
    def delete(self, tableName, Id):
        assert (isinstance(tableName, str) and ('!--' not in tableName)), 'Invalid tablename specified'
        assert (self.__hasTable(tableName)), 'Table: {}, does not exist!'.format(tableName)
        query = 'DELETE FROM {0} WHERE id=?;'.format(tableName)
        print(query)
        self.command.execute(query, Id)
        
    def select(self, tableName, parameters='*', Id=0):
        assert (isinstance(Id, int)), 'Not an int'
        assert (isinstance(tableName, str) and ('!--' not in tableName)), 'Invalid tablename specified'
        assert (self.__hasTable(tableName)), 'Table: {}, does not exist!'.format(tableName)
        query = ''
        if Id <= 0:
            query = 'SELECT {0} FROM {1};'.format(parameters, tableName)
            rows = self.command.execute(query, [])
            return rows
        else:
            query = 'SELECT {0} FROM {1} WHERE id={2};'.format(parameters, tableName, Id)
            rows = self.command.execute(query, [])
            return rows
    
    def dispose(self):
        self.command.closeConnection()

if __name__ == "__main__":
#==============================================================================
#     db= Database('FinplanDB.db')
#     rowid = db.insert('Friends', 'Name, Age', ['Sean', 1])
#     rowid = db.insert(tableName='Friends', parameters='Name, Age', values=['Sean', 1])
#     print(rowid)
#     rows = db.select('Friends')
#     for row in rows:
#         print(row)
#     db.update(tableName='Friends', parameters='Name, Age', values=['Tom', 2], Id=23)
#     rows = db.select('Friends')
#     for row in rows:
#         print(row)
#     db.delete('Friends',Id=[6])
#     rows = db.select('Friends', Id=18)
#     for row in rows:
#         print(row)
#     db.dispose()
#==============================================================================

    db= Client('FinplanDB.db', test=True)
    db.insertClient('Sean', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890')
    print(db.lastRowId())
    db.insertClient('Anita', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890')
    print(db.lastRowId())
    db.insertClient('Breege', 'Duignan', '31/12/1982', 'M', 'Home', 'Home', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890')
    print(db.lastRowId())
    db.updateClient('Tom', 'Duignan', '31/12/1982', 'M', 'HouseStreet', 'Area', 'Athlone', 'Roscommon', 'B2456YO', 'email@gmail.com', '0908723412', '0834567890', 3)
    rows = db.getAllClients()
    for row in rows:
        print(row)
    print(db.lastRowCount())
    rows = db.getAllClients('firstname,lastname,clientId')
    for row in rows:
        print(row)
    db.deleteClient(1)
    rows = db.getAllClients('firstname,lastname,clientId')
    for row in rows:
        print(row)
    print(db.lastRowCount())

    db.dispose()
